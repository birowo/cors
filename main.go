package main

import (
	"io"
	"log"
	"os"
	"sync"

	"github.com/valyala/fasthttp"
)

type Hosts map[string]fasthttp.RequestHandler

func (hs Hosts) Handler(ctx *fasthttp.RequestCtx) {
	if f, ok := hs[string(ctx.Host())]; ok {
		f(ctx)
	}
}
func main() {
	rootHtm := sync.Pool{New: func() any {
		f, _ := os.Open("root.htm")
		return f
	}}
	host := "172.22.111.186:8080"
	origin := "http://" + host + ""
	route := Hosts{
		host: func(ctx *fasthttp.RequestCtx) {
			ctx.SetContentType("text/html; charset=utf-8")
			htm_ := rootHtm.Get()
			defer rootHtm.Put(htm_)
			htm := htm_.(*os.File)
			htm.Seek(0, 0)
			_, err := io.Copy(ctx, htm)
			if err != nil {
				log.Println(err)
			}
		},
		"localhost:8080": func(ctx *fasthttp.RequestCtx) {
			if string(ctx.Request.Header.Peek("Origin")) == origin { //sebaiknya origin header di-validasi
				ctx.Response.Header.Set("Access-Control-Allow-Origin", origin) // or "*" for all origin
				ctx.Write([]byte("CORS sukses"))
			}
		},
	}
	const PORT = ":8080"
	fasthttp.ListenAndServe(PORT, route.Handler)
}
