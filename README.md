cors terjadi kalau FE dari suatu domain / ip-address mengakses BE di domain / ip-address lain.

di contoh ini implemen web server app. yang jalan di wsl ubuntu, diakses dari client(browser) yang jalan di mswindows.

alamat remote network wsl ubuntu (172.22.111.186 di contoh ini) didapat dari perintah ifconfig.

kalau web app. dan browser sama-sama jalan di local network umumnya pakai alamat 127.0.0.1 .

referensi dari MDN sudah cukup jelas : https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

![](https://gist.github.com/assets/21541959/6dd60b50-8cec-4160-b65d-cd382aa57783)
![](https://gist.github.com/assets/21541959/93293379-3e73-470a-8c87-82632ab106e2)
![](https://gist.github.com/assets/21541959/b720c247-40ef-40a5-b43d-baa79e18fccb)